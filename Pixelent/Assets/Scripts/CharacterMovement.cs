﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

	[SerializeField] private GameObject _mCharacter;


	private Vector3 _mDirection = Vector3.right;


	void Start()
	{
		InvokeRepeating("ChangePosition", 0, 1);
	}


	void Update()
	{
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			_mDirection = Vector3.up;
		}
		else if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			_mDirection = Vector3.right;
		}
		else if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			_mDirection = Vector3.down;
		}
		else if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			_mDirection = Vector3.left;
		}
	}

	private void ChangePosition()
	{
		transform.position += _mDirection;

		var newX = Mathf.Clamp(transform.position.x, -7, 7);
		var newY = Mathf.Clamp(transform.position.y, -10, 16);
		transform.position = new Vector3(newX, newY);
	}

}
